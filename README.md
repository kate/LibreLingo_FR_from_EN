# LibreLingo-FR-from-EN

[![status-badge](https://ci.codeberg.org/api/badges/13982/status.svg)](https://ci.codeberg.org/repos/13982)

LibreLingo [French course](https://librelingo.app/course/french-from-english) for English speakers

> ⚠️ Please note: This course is still under active development and contains some errors.

<img src="https://codeberg.org/kate/LibreLingo_FR_from_EN/raw/branch/master/images/preview.png" width="600">

## About LibreLingo (main software)
LibreLingo's mission is to create a modern language-learning platform that is owned by the community of its users. All software is licensed under AGPLv3, which guarantees the freedom to run, study, share, and modify the software. Course authors are encouraged to release their courses with free licenses.

## Differences between LibreLingo and LibreLingo Community
LibreLingo is the original project of Dániel Kántor. Unfortunately, LibreLingo's software stopped working because of Svelte problems that haven't been fixed for two years. In the meantime Dániel also decided to use another framework. So the project was paused until 2025, when Gregorio forked it, fixed the Svelte Issues (and other bugs), and revived it for the community. The LibreLingo Community was born. This also means that this course only works with LibreLingo Community at the moment, as LibreLingo is still broken. 

## About this french course dataset
It's licensed under Attribution-NonCommercial-ShareAlike 4.0 International
Please refer to this repository https://codeberg.org/kate/LibreLingo_FR_from_EN and "Kate". This is enough. 
