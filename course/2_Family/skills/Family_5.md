# 👵 👴 👴  la belle-famille (step-family) 👵 👴 👴  

This course is all about family.  
Ce cours concerne la famille.  

**Here is what you'll learn in this lesson:**  
*Voici ce que vous apprendrez dans cette leçon:*

## Vocabulary 
🇺🇸 goddaughter   |  🇫🇷 filleule     
🇺🇸 godfather     |  🇫🇷 parrain     
🇺🇸 godmother     |  🇫🇷 marraine     
🇺🇸 the-eldest    |  🇫🇷 l’aîné / l’aînée       
🇺🇸 younger       |  🇫🇷 le cadet / la cadette       
🇺🇸 the youngest  |  🇫🇷 le benjamin / la benjamine      
🇺🇸 twins         |  🇫🇷 les jumeaux / les jumelles      
🇺🇸 triplets      |  🇫🇷 les triplés / les triplées      


