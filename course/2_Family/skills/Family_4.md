# 👵 👴 👴  la belle-famille (step-family) 👵 👴 👴  

This course is all about family.  
Ce cours concerne la famille.  

**Here is what you'll learn in this lesson:**  
**Voici ce que vous apprendrez dans cette leçon:**  

## Vocabulary 
🇺🇸 step-son or son-in-law           |  🇫🇷 le beau-fils     
🇺🇸 step-daugther or daugther-in-law |  🇫🇷 la belle-fille     
🇺🇸 father-in-law or stepfather      |  🇫🇷 le neveu     
🇺🇸 mother-in-law or stepmother      |  🇫🇷 la belle-mère     
🇺🇸 half-brother or stepbrother      |  🇫🇷 le demi-frère     
🇺🇸 half-sister or stepsister        |  🇫🇷 la demi-soeur    
🇺🇸 brother-in-law                   |  🇫🇷 le beau-frère     
🇺🇸 sister-in-law                    |  🇫🇷 la belle-soeur    


