# 👵 👴 👴  Famille 👵 👴 👴  

This course is all about family. 
Ce cours concerne la famille.    

**Here is what you'll learn in this lesson:**    
**Voici ce que vous apprendrez dans cette leçon:**    
  
## Vocabulary   
🇺🇸 the family   | 🇫🇷 la famille    
🇺🇸 the daugther | 🇫🇷 la fille    
🇺🇸 the son      | 🇫🇷 le fils    
🇺🇸 the papa     | 🇫🇷 le père    
🇺🇸 the papa     | 🇫🇷 le papa    
🇺🇸 the mum      | 🇫🇷 la maman    
🇺🇸 the mother   | 🇫🇷 la mère    
🇺🇸 the brother  | 🇫🇷 le frère    
🇺🇸 the sister   | 🇫🇷 la soeur    
🇺🇸 the girl     | 🇫🇷 la fille    
🇺🇸 the boy      | 🇫🇷 le garçon    
