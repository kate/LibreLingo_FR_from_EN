# Colors 

This course is all about colors. 
Ce cours est consacré aux couleurs. 

**Here is what you'll learn in this lesson:**    
**Voici ce que vous apprendrez dans cette leçon:**    
  
## Vocabulary       
🇺🇸 red          | 🇫🇷 rouge       
🇺🇸 noir         | 🇫🇷 black       
🇺🇸 blanches     | 🇫🇷 white       