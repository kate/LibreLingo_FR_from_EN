# 🇫🇷 Radonnée 🇫🇷, 

**Nice that you'd like to learn some french.**  
*C'est bien que vous souhaitiez apprendre le français.*  

**In this lesson we will learn hiking vocabulary**  
*Dans cette leçon, nous allons apprendre le vocabulaire de la randonnée*    

## Vocabulary 
🇺🇸 Hiking         | 🇫🇷 Radonnée  

🇺🇸 the sign       | 🇫🇷 la pancarte  

🇺🇸 the mountain   | 🇫🇷 la montagne  

🇺🇸 bench          | 🇫🇷 banc  
 
🇺🇸 high           | 🇫🇷 haute  
 
🇺🇸 une carte      | 🇫🇷 a map  

