# 🇫🇷 Bricolage et jardinage 🇫🇷, 

**Nice that you'd like to learn some french.**  
*C'est bien que vous souhaitiez apprendre le français.*  

**In this lesson we will learn DIY and gardening vocabulary**  
*Dans cette leçon, nous apprendrons le vocabulaire du jardinage et du bricolage*    

## Vocabulary 
🇺🇸 plants         | 🇫🇷 plantes  

🇺🇸 plants         | 🇫🇷 bricolage  

🇺🇸 garden         | 🇫🇷 jardin  

🇺🇸 gardening      | 🇫🇷 jardinage  
 
🇺🇸 hammer         | 🇫🇷 marteau  

