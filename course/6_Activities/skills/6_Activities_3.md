# 🇫🇷 Au cinema 🇫🇷, 

**Nice that you'd like to learn some french.**  
*C'est bien que vous souhaitiez apprendre le français.*  

**In this lesson we will learn vocabulary to talk about going to the movies**  
*Dans cette leçon, nous allons apprendre du vocabulaire pour parler d'aller au cinéma*  

## Vocabulary 
🇺🇸 cinema         | 🇫🇷 cinéma  

🇺🇸 cheesy         | 🇫🇷 rigard

🇺🇸 comedies       | 🇫🇷 comédies

🇺🇸 meilleur       | 🇫🇷 best

🇺🇸 tickets        | 🇫🇷 tickets
    

