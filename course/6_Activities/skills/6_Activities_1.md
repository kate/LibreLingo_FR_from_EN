# 🇫🇷 Le Sport 🇫🇷, 

**Nice that you'd like to learn some french.**  
*C'est bien que vous souhaitiez apprendre le français.*  

**In this lesson we will learn vocabulary to talk about Sport.**  
*Dans cette leçon, nous allons apprendre du vocabulaire pour parler du sport.* 

## Vocabulary 
🇺🇸 dance                | 🇫🇷 danse  

🇺🇸 running              | 🇫🇷 courir

🇺🇸 swimmiing            | 🇫🇷 natation

🇺🇸 fast                 | 🇫🇷 vite

🇺🇸 basketball           | 🇫🇷 basket-ball
    
