# 🇫🇷 Faire la musique 🇫🇷, 

**Nice that you'd like to learn some french.**  
*C'est bien que vous souhaitiez apprendre le français.*  

**In this lesson we will learn vocabulary to talk about making music.**  
*Dans cette leçon, nous allons apprendre le vocabulaire dont vous avez besoin pour parler de faire de la musique.*   

## Vocabulary 
🇺🇸 the guitar           | 🇫🇷 la guitare  

🇺🇸 the piano            | 🇫🇷 la piano

🇺🇸 the electric guitar  | 🇫🇷 la guitar électrique

🇺🇸 violin               | 🇫🇷 le geige

🇺🇸 drum                 | 🇫🇷 tambour
    
🇺🇸 triangle             | 🇫🇷 triangle
     
🇺🇸 trumpet              | 🇫🇷 trompette
 
🇺🇸 playing              | 🇫🇷 jouer

