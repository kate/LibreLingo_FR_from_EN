# 🇫🇷 Faire du shopping 🇫🇷, 

**Nice that you'd like to learn some french.**  
*C'est bien que vous souhaitiez apprendre le français.*  

**In this lesson we will learn vocabulary to talk about shopping**  
*Dans cette leçon, nous apprendrons le vocabulaire pour parler des achats.* 

## Vocabulary 
🇺🇸 cash           | 🇫🇷 espèces  

🇺🇸 credit card    | 🇫🇷 carte de crédit 

🇺🇸 acheter        | 🇫🇷 buying  


