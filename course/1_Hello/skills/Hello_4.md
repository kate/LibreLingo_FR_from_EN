# 🇫🇷 Hello et Salut 🇫🇷, 

After finishing this lesson you should be able to:

* Introduce yourself
* Ask for someones name
* Using basic words such as yes, no and thank you.

Après avoir terminé cette leçon, vous devriez être en mesure de :   

* Se présenter
* Demander le nom de quelqu'un
* Utiliser des mots de base tels que oui, non et merci.