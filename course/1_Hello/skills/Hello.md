# 🇫🇷 Hello et Salut 🇫🇷, 

**Nice that you'd like to learn some french.**  
*C'est bien que vous souhaitiez apprendre le français.*  

**We will start with an easy introduction.**  
*Nous commencerons par une introduction facile*.   

**To get you used to the french language,**  
*Pour vous habituer à la langue française,*  

**each sentence will be translated right below into french.**  
*chaque phrase sera traduite ci-dessous en français.*  

**Don't worry, if you do not understand everything right away.**  
*Ne vous inquiétez pas si vous ne comprenez pas tout tout de suite.*  

**Here is what you'll learn in this lesson:**  
* Introducing yourself   
* Greet others  

## Vocabulary 
🇺🇸 I am |  🇫🇷 Je suis

🇺🇸 My name is | 🇫🇷 Je m’appelle ...

🇺🇸 What’s your name? (informal) | 🇫🇷 Comment t’appelles-tu ?

🇺🇸  I live in ... | 🇫🇷 J’habite à ...

🇺🇸  I am ... years old | 🇫🇷 J’ai ...  ans.

🇺🇸 Hello | 🇫🇷 Salut

🇺🇸 Yes | 🇫🇷 Oui

🇺🇸 No | 🇫🇷 Non


