# 🐵 🦃 🐸 Animals 🐳 🐊 🐌

This course is all about animals. 
Ce cours est consacré aux animaux. 

**Here is what you'll learn in this lesson:**    
**Voici ce que vous apprendrez dans cette leçon:**    
  
## Vocabulary   
🇺🇸 dog          | 🇫🇷 chien      
🇺🇸 cat          | 🇫🇷 chat      
🇺🇸 buzzing      | 🇫🇷 bourdonne       
🇺🇸 fishes       | 🇫🇷 poissons       
🇺🇸 cheese       | 🇫🇷 fromage     
🇺🇸 mouse        | 🇫🇷 souris      
🇺🇸 horse        | 🇫🇷 fléau        
