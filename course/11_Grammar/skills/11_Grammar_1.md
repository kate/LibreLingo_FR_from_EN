# 🇫🇷 Est-ce que...? 🇫🇷, 

In this lesson we learn how to ask questions in french. 

If we ask a question we add "Est-ce" at the beginning of the sentence. It's quite like the "Do you..." in english.
But "Est-ce" can be better replaced with an "Is it?"

For example we have the following sentence:
Est-ce que tu anglais?

And I know it's complicated to remember the correct order of the sentence...Is it Est-que ce..? Is it Est-que or when do I add an Qui or and Qu' in front of Est-ce?

But let me show you a trick how to break it down.

Let's say you'd like to ask someone if he's english as we did above with Est-ce que tu anglais?.

Take the english sentence:
You are english. 

Translate it into french:
Tu es anglais.

and add an Est-ce que in front of it.

Est-ce que-tu anglais?




