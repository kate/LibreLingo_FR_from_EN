# 🇫🇷 Numéros III - 11 - 20🇫🇷, 

**Nice that you'd like to learn some french.**  
*C'est bien que vous souhaitiez apprendre le français.*  

**We learn in this lesson to count from one to ten.**  
*Nous apprenons dans cette leçon à compter de un à dix.*  

## Vocabulary 
🇺🇸 Eleven |  🇫🇷 Un  
  
🇺🇸 Twelve | 🇫🇷 Deux  
  
🇺🇸 Thirteen | 🇫🇷 Trois  
  
🇺🇸 Fourteen | 🇫🇷 Quatre  
  
🇺🇸  | 🇫🇷 Cinq  
  
🇺🇸 Sixteen | 🇫🇷 Six  
   
🇺🇸 Seventeen | 🇫🇷 Sept  
  
🇺🇸 Eightteen | 🇫🇷 Huit  
  
🇺🇸 Nineteen | 🇫🇷 Neuf  
  
🇺🇸 Twenty | 🇫🇷 Dix  

 
