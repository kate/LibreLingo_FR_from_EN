# 🇫🇷 Weather 🇫🇷, 

**Nice that you'd like to learn some french.**  
*C'est bien que vous souhaitiez apprendre le français.*  

**Wir lernen in dieser Lektion etwas über das Wetter.**  
*Nous allons apprendre quelque chose sur le temps dans cette leçon.*  

## Vocabulary 
🇺🇸 Clouds | 🇫🇷 Nuages  

🇺🇸 Sky    | 🇫🇷 Ciel  

🇺🇸 Wind   | 🇫🇷 Vent  

🇺🇸 Sun    | 🇫🇷 Soleil  

🇺🇸 Moon   | 🇫🇷 Lune  

🇺🇸 Fog    | 🇫🇷 Brouillard  

🇺🇸 Rain   | 🇫🇷 Pluie  

🇺🇸 Hot    | 🇫🇷 chaud  

🇺🇸 Cold   | 🇫🇷 froid  
