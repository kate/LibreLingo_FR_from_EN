# 🇫🇷 Weather II 🇫🇷, 

**Nice that you'd like to learn some french.**  
*C'est bien que vous souhaitiez apprendre le français.*  

**Wir lernen in dieser Lektion etwas über das Wetter.**  
*Nous allons apprendre quelque chose sur le temps dans cette leçon.*  

## Vocabulary 
🇺🇸 the moon | 🇫🇷 la lune  

🇺🇸 hail     | 🇫🇷 grêle

🇺🇸 tornado  | 🇫🇷 tornade

🇺🇸 temps    | 🇫🇷 weather


