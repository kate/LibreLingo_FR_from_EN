# Plurals

This course is all about plurals. 
Ce cours est consacré aux pluriels. 

**Here is what you'll learn in this lesson:**    
**Voici ce que vous apprendrez dans cette leçon:**    
  
## Vocabulary   
🇺🇸 house, houses     | 🇫🇷 maison, maisons        
🇺🇸 castle, castles   | 🇫🇷 château, châteaux         