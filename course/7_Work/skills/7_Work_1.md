# 🇫🇷 Le travail 🇫🇷, 

In this french lesson we talk about Work. We learn job titles, phone conversations and office talk.     

Dans cette leçon de français, nous parlons du travail. Nous apprenons les titres de postes, les conversations téléphoniques et le langage de bureau. 


## Vocabulary 
🇺🇸 unemployed      | 🇫🇷 chômage  

🇺🇸 manager         | 🇫🇷 gérant  

🇺🇸 work            | 🇫🇷 travailleur    

🇺🇸 engineer        | 🇫🇷 ingénieur  

🇺🇸 enterpreneuer   | 🇫🇷 enterpreneuer  