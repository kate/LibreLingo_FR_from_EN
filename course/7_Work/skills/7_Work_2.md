# 🇫🇷 Le travail 🇫🇷, 

In this french lesson we talk about Work. We learn job titles, phone conversations and office talk.     

Dans cette leçon de français, nous parlons du travail. Nous apprenons les titres de postes, les conversations téléphoniques et le langage de bureau. 


## Vocabulary 
🇺🇸 fast            | 🇫🇷 vite   

🇺🇸 occupation      | 🇫🇷 métier  

🇺🇸 company         | 🇫🇷 enterprise       

🇺🇸 engineer        | 🇫🇷 ingénieur   

🇺🇸 opportunity     | 🇫🇷 opportunité  

🇺🇸 search          | 🇫🇷 cherche  

🇺🇸 work            | 🇫🇷 travail  

🇺🇸 internship      | 🇫🇷 stage  

🇺🇸 job             | 🇫🇷 travail  

🇺🇸 employment      | 🇫🇷 d'emploi  

🇺🇸 self-employed   | 🇫🇷 travailleur indépendant   

🇺🇸 career          | 🇫🇷 carrière  