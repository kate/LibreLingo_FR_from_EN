# Le Légume, 

**Here is what you'll learn in this lesson:**  
*Vegetable names in french*   

## Vocabulary 

🇺🇸 garlic        |  🇫🇷 l’ail (m)      
🇺🇸 asparagus     |  🇫🇷 l’asperge (f)     
🇺🇸 carrot        |  🇫🇷 la carotte      
🇺🇸 cucumber      |  🇫🇷 le concombre    
🇺🇸 onion         |  🇫🇷 l'oignon (m)     
🇺🇸 zucchini      |  🇫🇷 la courgette      
🇺🇸 shallot       |  🇫🇷 l’échalotte (f)     
🇺🇸 spinach       |  🇫🇷 l’épinard (m)     
🇺🇸 sweetcorn     |  🇫🇷 le maïs doux     
🇺🇸 chilli pepper |  🇫🇷 le piment     
🇺🇸 pumpkin       |  🇫🇷 le potimarron      




