# 🍇🍈 Le Fruits 🍊🍉 ,  

**Here is what you'll learn in this lesson:**  
* Fruit names in french   

## Vocabulary 

🇺🇸 Lingonberry   |  🇫🇷 Airelle     
🇺🇸 Raspberry     |  🇫🇷 Framboise     
🇺🇸 Star fruit    |  🇫🇷 Carambola     
🇺🇸 Strawberry    |  🇫🇷 fraise    
🇺🇸 melon         |  🇫🇷 melon     
🇺🇸 Watermelon    |  🇫🇷 Pastèque     
🇺🇸 mango         |  🇫🇷 la mangue     
🇺🇸 olive         |  🇫🇷 l'olive (f)    

