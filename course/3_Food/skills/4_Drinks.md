## Drinks

🇺🇸 In this lesson we learn about french drinks to prepare you to order drinks at a restaurant, café or bar.  

🇫🇷 Dans cette leçon, nous apprenons à connaître les boissons françaises pour vous préparer à commander des boissons dans un restaurant, un café ou un bar.  

🇺🇸 Water          | 🇫🇷 Eau     
🇺🇸 Orange Juice   | 🇫🇷 Jus d'orange Orange       
🇺🇸 Mineral water  | 🇫🇷 Eau minérale     
🇺🇸 Glass of water | 🇫🇷 Verre d'eau     
🇺🇸 Lemon juice    | 🇫🇷 Citron pressé     
🇺🇸 Milk           | 🇫🇷 Lait     
🇺🇸 Syrup          | 🇫🇷 Sirop     
🇺🇸 Coffee         | 🇫🇷 Café     
🇺🇸 Tea            | 🇫🇷 Thé      
🇺🇸 Beer           | 🇫🇷 Bière     
🇺🇸 Hot choclate   | 🇫🇷 Chocolat chaud       
🇺🇸 White wine     | 🇫🇷 Vin blanc      
🇺🇸 Champange      | 🇫🇷 Champagne     
🇺🇸 Red wine       | 🇫🇷 Vin rouge     
🇺🇸 Glass          | 🇫🇷 Verre     


