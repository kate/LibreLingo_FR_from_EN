# 🍇🍈 Le Fruits 🍊🍉 , 

**Here is what you'll learn in this lesson:**  
* Fruit names in french   

## Vocabulary 

🇺🇸 Clementine   |  🇫🇷 Clémentine      
🇺🇸 Grapefruit   |  🇫🇷 Pamplemousse     
🇺🇸 Orange       |  🇫🇷 Orange      
🇺🇸 Chery        |  🇫🇷 Cerise     
🇺🇸 Coconut      |  🇫🇷 Noix de coco       
🇺🇸 Mango        |  🇫🇷 Mangue      
🇺🇸 Banana       |  🇫🇷 Banane      
🇺🇸 Dragon fruit |  🇫🇷 Fruit du dragon       
🇺🇸 Lemon        |  🇫🇷 Citron      
🇺🇸 Lime         |  🇫🇷 Lime      
🇺🇸 Grape        |  🇫🇷 Raisin     
	


